# -*- coding: utf-8 -*-

from PyQt4.QtCore import Qt
from parameters import Params
from geo_utils import vector_utils as vutils
from qgis.core import *
from qgis.gui import *
import operator

tolerance = 0.01


class SmoothFeatureTool(QgsMapTool):

    def __init__(self, data_dock, iters, offset, enforce_topol):
        QgsMapTool.__init__(self, data_dock.iface.mapCanvas())

        self.data_dock = data_dock
        self.iface = data_dock.iface

        self.mouse_pt = None
        self.mouse_clicked = False

        self.iters = iters
        self.offset = offset
        self.enforce_topol = enforce_topol

    def canvasPressEvent(self, event):
        if event.button() == Qt.RightButton:
            self.mouse_clicked = False

        if event.button() == Qt.LeftButton:
            self.mouse_clicked = True

    def canvasMoveEvent(self, event):
        self.mouse_pt = self.toMapCoordinates(event.pos())

    def canvasReleaseEvent(self, event):
        if not self.mouse_clicked:
            return

        if event.button() == Qt.LeftButton:

            self.mouse_clicked = False

            layer = self.iface.activeLayer()
            if layer is None:
                self.iface.messageBar().pushWarning(
                    Params.plugin_name,
                    'No layer selected.')  # TODO: softcode
                return

            if not layer.isEditable():
                self.iface.messageBar().pushInfo(
                    Params.plugin_name,
                    'The selected layer is not editable.')  # TODO: softcode
                return

            self.mouse_pt = self.toLayerCoordinates(layer, event.pos())
            closest_fts = MapToolsUtils.find_clicked_feats(self.iface, self.mouse_pt, layer)
            if closest_fts is None:
                return
            closest_ft = closest_fts[0]

            old_geom = closest_ft.geometry()

            layer.beginEditCommand('Smooth line')

            old_pts = old_geom.asPolyline()

            # Find intersecting lines
            inters_lines = {}
            cand_fts = layer.getFeatures(QgsFeatureRequest().setFilterRect(old_geom.boundingBox()))
            for cand_ft in cand_fts:
                if cand_ft.id() == closest_ft.id():
                    continue
                if old_geom.intersects(cand_ft.geometry()):
                    cand_pts = cand_ft.geometry().asPolyline()
                    # Find points overlapping points
                    for cpt in range(len(cand_pts)):
                        for old_pt in old_pts:
                            if cand_pts[cpt].compare(old_pt, tolerance):
                                if cand_ft.id() in inters_lines:
                                    inters_lines[cand_ft.id()].append(cpt)
                                else:
                                    inters_lines[cand_ft.id()] = [cpt]

            # Smooth line
            new_line_pts = old_geom.smoothLine(old_geom.asPolyline(), self.iters, self.offset)
            # new_line_geom = QgsGeometry.fromPolyline(new_line_pts)

            # Modify intersecting lines
            if self.enforce_topol:
                for ints_line_ft_id, vertex_idxs in inters_lines.iteritems():
                    ints_line_ft = vutils.get_feats_by_id(layer, ints_line_ft_id)[0]
                    old_ints_geom = ints_line_ft.geometry()
                    old_ints_pts = old_ints_geom.asPolyline()
                    for vertex_idx in vertex_idxs:
                        new_line_geom = QgsGeometry.fromPolyline(new_line_pts)
                        new_vx_pos = new_line_geom.nearestPoint(QgsGeometry.fromPoint(old_ints_pts[vertex_idx])).asPoint()
                        # Check if on the new line geometry a vertex exists closed enough to new vertex position
                        v_found = False
                        for new_line_pt in new_line_pts:
                            if new_line_pt.compare(new_vx_pos, tolerance):
                                old_ints_pts[vertex_idx] = new_line_pt
                                v_found = True
                                break
                        if not v_found:
                            # Add a point in new line geom and move point of intersecting l ine
                            pt, side, aft = new_line_geom.closestSegmentWithContext(new_vx_pos)
                            new_line_pts.insert(aft, new_vx_pos)
                            old_ints_pts[vertex_idx] = new_vx_pos

                    layer.changeGeometry(ints_line_ft.id(), QgsGeometry.fromPolyline(old_ints_pts))

            new_line_geom = QgsGeometry.fromPolyline(new_line_pts)
            layer.changeGeometry(closest_ft.id(), new_line_geom)
            layer.endEditCommand()

            self.iface.mapCanvas().refresh()

    def activate(self):
        pass

    def deactivate(self):
        self.data_dock.btn_smooth.setChecked(False)

    def isZoomTool(self):
        return False

    def isTransient(self):
        return False

    def isEditTool(self):
        return True

    def reset_marker(self):
        self.outlet_marker.hide()
        self.canvas().scene().removeItem(self.outlet_marker)


class MapToolsUtils:
    def __init__(self):
        pass

    @staticmethod
    def find_clicked_feats(iface, mouse_pt, layer, feats_count=1):

        if type(layer) is not QgsVectorLayer:
            return

        width = iface.mapCanvas().mapUnitsPerPixel() * 4
        rect = QgsRectangle(mouse_pt.x() - width,
                            mouse_pt.y() - width,
                            mouse_pt.x() + width,
                            mouse_pt.y() + width)

        request = QgsFeatureRequest(rect)
        feats = layer.getFeatures(request)

        dists_d = {}
        for feat in feats:
             dists_d[feat] = feat.geometry().distance(QgsGeometry.fromPoint(mouse_pt))
             dist = feat.geometry().distance(QgsGeometry.fromPoint(mouse_pt))
             if dist > width / 2:
                 continue
             dists_d[feat] = dist

        sorted_dists = sorted(dists_d.items(), key=operator.itemgetter(1))
        closest_ft = sorted_dists[0][0:feats_count]
        return closest_ft
