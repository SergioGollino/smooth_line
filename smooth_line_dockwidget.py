# -*- coding: utf-8 -*-
"""
/***************************************************************************
 SmoothLineDockWidget
                                 A QGIS plugin
 Allow to smooth a single line with a clic and respects topology.
                             -------------------
        begin                : 2016-12-27
        git sha              : $Format:%H$
        copyright            : (C) 2016 by  Alberto De Luca for Tabacco Editrice
        email                : sergio.gollino@tabaccoeditrice.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os

from map_tools import SmoothFeatureTool
from PyQt4 import QtGui, uic
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from parameters import Params
from tools.validators import RegExValidators

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'smooth_line_dockwidget.ui'))


class SmoothLineDockWidget(QtGui.QDockWidget, FORM_CLASS):

    closingPlugin = pyqtSignal()

    def __init__(self, iface, parent=None):
        """Constructor."""
        super(SmoothLineDockWidget, self).__init__(parent)

        self.setupUi(self)

        self.iface = iface

        self.tool = None

        fra_main_lay = QVBoxLayout(self.fra_main)
        fra_main_lay.setContentsMargins(0, 0, 0, 0)
        fra_main_lay.setSpacing(0)

        self.fra_top = QFrame(self.fra_main)
        fra_top_lay = QFormLayout(self.fra_top)

        self.lbl_iterations = QLabel('Iterations:')
        self.cbo_iterations = QComboBox()
        fra_top_lay.addRow(self.lbl_iterations, self.cbo_iterations)

        self.lbl_offset = QLabel('Offset:')
        self.txt_offset = QLineEdit()
        fra_top_lay.addRow(self.lbl_offset, self.txt_offset)

        self.chk_topology = QCheckBox('Enforce topology')
        fra_top_lay.addWidget(self.chk_topology)

        self.fra_bottom = QFrame(self.fra_main)
        fra_bottom_lay = QFormLayout(self.fra_bottom)

        self.btn_smooth = QPushButton('Smooth')
        fra_bottom_lay.addWidget(self.btn_smooth)
        fra_bottom_lay.setContentsMargins(0, 0, 0, 0)

        fra_main_lay.addWidget(self.fra_top)
        fra_main_lay.addWidget(self.fra_bottom)
        fra_main_lay.addSpacerItem(QSpacerItem(0, 10, QSizePolicy.Minimum, QSizePolicy.Expanding))

        self.setup()
        self.initialize()

    def setup(self):

        self.setWindowTitle(Params.plugin_name)

        # Smooth selected lines
        self.btn_smooth.setCheckable(True)
        self.btn_smooth.pressed.connect(self.btn_smooth_activated)
        for i in range(1, 5):
            self.cbo_iterations.addItem(str(i), i)

        self.txt_offset.setText(str(0.25))
        self.txt_offset.setValidator(RegExValidators.get_pos_decimals())

    def initialize(self):
        self.chk_topology.setChecked(True)

    def closeEvent(self, event):
        self.closingPlugin.emit()
        event.accept()

    def btn_smooth_activated(self):

        iters = self.cbo_iterations.itemData(self.cbo_iterations.currentIndex())
        offset = float(self.txt_offset.text())

        enforce_topol = self.chk_topology.isChecked()
        self.tool = SmoothFeatureTool(self, iters, offset, enforce_topol)
        self.iface.mapCanvas().setMapTool(self.tool)
        self.set_cursor(Qt.CrossCursor)

    def set_cursor(self, cursor_shape):
        cursor = QCursor()
        cursor.setShape(cursor_shape)
        self.iface.mapCanvas().setCursor(cursor)