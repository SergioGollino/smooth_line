from PyQt4.QtCore import QRegExp
from PyQt4.QtGui import QRegExpValidator

regex_number_pos_decimals = '^[0-9]\d*(\.\d+)?$'
regex_number_pos_neg_decimals = '^-?[0-9]\d*(\.\d+)?$'
regex_number_pos_int = '^[0-9]\d*$'
regex_number_pos_int_no_zero = '^[1-9]\d*$'
regex_time_hh_mm = '^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$'


class RegExValidators:

    def __init__(self):
        pass

    @staticmethod
    def get_pos_decimals():
        reg_ex = QRegExp(regex_number_pos_decimals)
        validator = QRegExpValidator(reg_ex)
        return validator

    @staticmethod
    def get_pos_neg_decimals():

        reg_ex = QRegExp(regex_number_pos_neg_decimals)
        validator = QRegExpValidator(reg_ex)
        return validator

    @staticmethod
    def get_pos_int():

        reg_ex = QRegExp(regex_number_pos_int)
        validator = QRegExpValidator(reg_ex)
        return validator

    @staticmethod
    def get_pos_int_no_zero():

        reg_ex = QRegExp(regex_number_pos_int_no_zero)
        validator = QRegExpValidator(reg_ex)
        return validator

    @staticmethod
    def get_time_hh_mm():

        reg_ex = QRegExp(regex_time_hh_mm)
        validator = QRegExpValidator(reg_ex)
        return validator